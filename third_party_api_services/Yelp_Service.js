var yelp = require('yelp-fusion');
var fs = require('fs');
var event = require('events');
var util = require('util');


function Yelp_Service() {
    var key = fs.readFileSync('./third_party_api_services/keys/yelp.txt');
    this.client = yelp.client(key.toString());
}


Yelp_Service.prototype.search = function (search_term) {
    var self = this;

    this.client.search(
        search_term
    ).then(function (response) {
        var result = [];
        var businesses = response.jsonBody.businesses;
        for(var i = 0; i < businesses.length; i++) {
            var business = {
                id: businesses[i].id,
                name: businesses[i].name,
                phone: businesses[i].phone,
                location: businesses[i].location,
                coordinates: businesses[i].coordinates,
                distance: businesses[i].distance,
                categories: businesses[i].categories,
                rating: businesses[i].rating,
                price: businesses[i].price,
                is_closed: businesses[i].is_closed,
                image_url: businesses[i].image_url,
            };

            result.push(business);
        }

        self.emit("finish getting restaurants", result);

    }).catch(function (error) {
        self.emit("finish getting restaurants", []);
        console.log(error);
    });
};


Yelp_Service.prototype.get_restaurant_detail = function (restaurant_id) {
    var self = this;

    this.client.business(restaurant_id).then(
        function (response) {
            self.emit("finish getting restaurant detail", response.jsonBody);
        }
    ).catch(function (error) {
        self.emit("finish getting restaurant detail", []);
        console.log(error);
    })
};


Yelp_Service.prototype.get_restaurant_reviews = function (restaurant_id) {
    var self = this;

    this.client.reviews(restaurant_id).then(
        function (response) {
            self.emit("finish getting restaurant reviews", response.jsonBody.reviews);
        }
    ).catch(function (error) {
        self.emit("finish getting restaurant reviews", []);
        console.log(error);
    })
};


util.inherits(Yelp_Service, event);
module.exports = Yelp_Service;
