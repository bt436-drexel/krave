var express = require('express');
var router = express.Router();
var yelp_service = require('../third_party_api_services/Yelp_Service.js');

// setup third party services
var yelp = new yelp_service();


/* GET restaurant search. */
router.get('/search', function(req, res) {
    var search_term = {
        term: req.query.term === undefined ? "" : req.query.term,
        location: req.query.location === undefined ? "" : req.query.location,
        latitude: req.query.latitude === undefined ? "" : req.query.latitude,
        longitude: req.query.longitude === undefined ? "" : req.query.longitude,
        radius: Number(req.query.radius) === undefined ? "" : Number(req.query.radius),
        categories: req.query.categories === undefined ? "" : req.query.categories,
        open_now: req.query.open_now === undefined ? "" : req.query.open_now,
        limit: req.query.limit === undefined ? "" : req.query.limit
    };

    yelp.once("finish getting restaurants", function (msg) {
        res.send(msg);
    });

    yelp.search(search_term);
});


router.get('/detail', function (req, res) {
    yelp.once("finish getting restaurant detail", function (msg) {
        res.send(msg);
    });

    yelp.get_restaurant_detail(req.query.id === undefined ? "" : req.query.id);
});


router.get('/review', function (req, res) {
    yelp.once("finish getting restaurant reviews", function (msg) {
        res.send(msg);
    });

    yelp.get_restaurant_reviews(req.query.id === undefined ? "" : req.query.id);
});

module.exports = router;
